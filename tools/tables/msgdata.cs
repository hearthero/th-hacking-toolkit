﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th_hack_tools.tools.tables
{
    static class msgdata_helper
    {

        static Dictionary<int, string> read_archive(string[] strings, int start = 0, int items = 1)
        {
            Dictionary<int, string> NameTable = new Dictionary<int, string>();

            int index = 0;
            for (int i = start; i < start + items; i++)
            {
                NameTable.Add(index, strings[i]);
                index++;
            }

            return NameTable;

        }

        static public Dictionary<int, string> get_character_strings(string[] strings)
        {
            Dictionary<int, string> Characters = read_archive(strings, 1156, 45);
            Characters[0] = Characters[0] + " (Male)";
            Characters[1] = Characters[1] + " (Female)";
            return Characters;
        }

        static public Dictionary<int, string> get_class_strings(string[] strings)
        {
            return read_archive(strings, 3452, 90);
        }

        static public Dictionary<int, string> get_crest_strings(string[] strings)
        {
            Dictionary<int, string> Crests = read_archive(strings, 9586, 48);
            Crests.Add(0xFF, "No Crest");
            return Crests;
        }

        static public Dictionary<int, string> get_spell_strings(string[] strings)
        {
            Dictionary<int, string> Spells = read_archive(strings, 7832, 38);
            Spells.Add(0xFF, "No Spell");
            return Spells;
        }
        static public Dictionary<int, string> get_skill_strings(string[] strings)
        {
            Dictionary<int, string> Skills = read_archive(strings, 7232, 238);
            Skills.Add(0xFF, "No Skill");
            return Skills;
        }

        static public Dictionary<int, string> get_art_strings(string[] strings)
        {
            Dictionary<int, string> Arts = read_archive(strings, 6010, 77);
            Arts.Add(0xFF, "No Art");
            return Arts;
        }

        static public Dictionary<int, string> get_item_strings(string[] strings)
        {
            Dictionary<int, string> Items = read_archive(strings, 4622, 26);
            Items.Add(0xFF, "No Item");
            return Items;
        }

    }
}
