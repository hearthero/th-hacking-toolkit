﻿using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th_hack_tools
{
    // This class should be used for common functions that are used between various classes
    // It will mostly consist of byte and streaming functions

    public static class th_utils
    {
        /// <summary>
        /// Reads n bytes from a FileStream starting at offset x. 
        /// </summary>
        /// <param name="fs">The FileStream to read from.</param>
        /// <param name="offset">The offset to start from.</param>
        /// <param name="length">The length of the byte array to return.</param>
        /// <param name="origin">Defines if offset is relative or starts from beginning</param>
        /// <returns></returns>
        public static byte[] read_seek(FileStream fs, long offset, int length, SeekOrigin origin = SeekOrigin.Begin)
        {
            byte[] buffer = new byte[length];
            fs.Seek(offset, origin);
            fs.Read(buffer, 0, length);
            return buffer;
        }

        public static int read_seek_int32(FileStream fs, long offset = 0, SeekOrigin origin = SeekOrigin.Current)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer, 0, 4);

            return BitConverter.ToInt32(buffer, 0);
        }

        public static uint read_seek_uint32(FileStream fs, long offset = 0, SeekOrigin origin = SeekOrigin.Current)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer, 0, 4);

            return BitConverter.ToUInt32(buffer, 0);
        }

        public static long read_seek_int64(FileStream fs, long offset = 0, SeekOrigin origin = SeekOrigin.Current)
        {
            byte[] buffer = new byte[8];
            fs.Read(buffer, 0, 8);

            return BitConverter.ToInt64(buffer, 0);
        }

        /// <summary>
        /// Skips n bytes where n is the number of bytes until the residual value equals zero. Useful for all files that use common buffer sizes.
        /// </summary>
        /// <param name="fs">The FileStream to read from.</param>
        /// <param name="mod">The modulus to use.</param>
        public static void modulus_skip(FileStream fs, long mod, long offset = 0)
        {
            byte[] buffer = new byte[1];
            while ((fs.Position - offset) % mod != 0)
            {
                fs.Read(buffer, 0, 1);
            }
        }

        /// <summary>
        /// Reads a FileStream, decompresses zlib data and returns an uncompressed byte array.
        /// </summary>
        /// <param name="originalFileStream">The FileStream to read from.</param>
        /// <param name="length">The length of the compressed data.</param>
        /// <returns></returns>
        public static byte[] decompress_stream(FileStream originalFileStream, int length)
        {
            byte[] buffer = new byte[length];
            originalFileStream.Read(buffer, 0, length);

            MemoryStream compressedStream = new MemoryStream(buffer);
            MemoryStream decompressedStream = new MemoryStream();
            buffer = null;

            using (ZlibStream decompressionStream = new ZlibStream(compressedStream, CompressionMode.Decompress))
            {

                int bufferSize = 1024;
                byte[] buffer_z = new byte[bufferSize];

                int bytesRead = -1;

                while ((bytesRead = decompressionStream.Read(buffer_z, 0, bufferSize)) > 0)
                {
                    decompressedStream.Write(buffer_z, 0, bufferSize);
                }

                decompressionStream.Close();

            }
            return decompressedStream.ToArray();
        }

        public static void decompress_file(FileStream originalFileStream, string new_path, long offset=0)
        {
            originalFileStream.Seek(offset, SeekOrigin.Begin);
            uint chunk_size = read_seek_uint32(originalFileStream);
            uint chunks = read_seek_uint32(originalFileStream);
            uint size = read_seek_uint32(originalFileStream);

            List<uint> chunk_sizes = new List<uint>();

            for (int i=0;i< chunks;i++)
            {
                chunk_sizes.Add(read_seek_uint32(originalFileStream));
            }

            modulus_skip(originalFileStream, 128);

            using (FileStream decompressedFile = new FileStream(new_path, FileMode.Create, FileAccess.Write))
            {
                for (int i = 0; i < chunks; i++)
                {
                    long current_offset = originalFileStream.Position;
                    int current_length = read_seek_int32(originalFileStream);
                    byte[] decompressed;

                    if (current_length == chunk_sizes[i] - 4)
                        decompressed = decompress_stream(originalFileStream, current_length);
                    else
                        decompressed = read_seek(originalFileStream, 0, (int)chunk_sizes[i], SeekOrigin.Current);

                    if (decompressed.Length + decompressedFile.Length < size)
                        decompressedFile.Write(decompressed, 0, decompressed.Length);
                    else
                        decompressedFile.Write(decompressed, 0, (int)(size - decompressedFile.Length));

                    modulus_skip(originalFileStream, 128, current_offset);
                }

                decompressedFile.Close();
            }
            originalFileStream.Close();
        }

        public static string analyze_extension(string filename)
        {
            string header = "";
            string ext = ".bin";

            using (FileStream analyze_stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] buffer = new byte[4];
                analyze_stream.Read(buffer, 0, 4);
                analyze_stream.Close();
                header = Encoding.ASCII.GetString(buffer);
            }

            switch (header)
            {
                case "SARC":
                    ext = ".arc";
                    break;
                case "SPKG":
                    ext = ".spkg";
                    break;
                case "GT1G":
                    ext = ".g1t";
                    break;
                case "KTSR":
                    ext = ".ktsl2stbin";
                    break;
                case "\x1A\x45\xDF\xA3":
                    ext = ".webm";
                    break;
            }

            return ext;
        }

    }
}
