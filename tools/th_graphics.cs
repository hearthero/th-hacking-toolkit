﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace th_hack_tools.tools
{

    public class GT1
    {

        FileStream fs;
        public List<THTexture> textures = new List<THTexture>();
        public uint texture_count = 0;
        public string file;

        public GT1(string path)
        {

            textures.Clear();
            file = path;
            fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[8];

            fs.Read(buffer, 0, 8);

            switch (Encoding.ASCII.GetString(buffer))
            {
                case "GT1G0600":
                case "GT1G1600":
                case "GT1G2600":

                    break;
                default:
                    throw new ArgumentException("Incorrect header.", "ERR_READ_GT1");
            }

            buffer = th_utils.read_seek(fs, 0x8, 4);
            uint file_length = BitConverter.ToUInt32(buffer, 0);

            buffer = th_utils.read_seek(fs, 0xC, 4);
            uint start_offset = BitConverter.ToUInt32(buffer, 0);

            buffer = th_utils.read_seek(fs, 0x10, 4);
            texture_count = BitConverter.ToUInt32(buffer, 0);

            // Loop through texture offset table

            buffer = th_utils.read_seek(fs, (int)start_offset, (int)texture_count * 4);

            for (int i = 0; i < buffer.Length; i += 4)
            {
                uint current_offset = BitConverter.ToUInt32(buffer, i) + start_offset;

                if (i == buffer.Length - 4)
                {
                    uint texture_length = file_length - current_offset;
                    textures.Add(new THTexture(fs, current_offset, texture_length));
                }
                else
                {
                    uint next_offset = BitConverter.ToUInt32(buffer, i + 4) + start_offset;
                    uint current_length = next_offset - current_offset;
                    textures.Add(new THTexture(fs, current_offset, current_length));
                }

            }

            fs.Close();

        }

        public MemoryStream DDS_Stream(int index)
        {
            fs = new FileStream(file, FileMode.Open, FileAccess.Read);
            THTexture texture = textures[index];
            byte[] buffer = new byte[texture.Length - 0x10];
            fs.Seek(texture.Offset + 0x10, SeekOrigin.Begin);
            fs.Read(buffer, 0, buffer.Length);

            List<byte> dds_raw = new List<byte>();
            dds_raw.AddRange(new byte[0xC] { 0x44, 0x44, 0x53, 0x20, 0x7C, 0x00, 0x00, 0x00, 0x07, 0x10, 0x08, 0x00 });
            dds_raw.AddRange(BitConverter.GetBytes((uint)texture.Width));
            dds_raw.AddRange(BitConverter.GetBytes((uint)texture.Height));
            dds_raw.AddRange(BitConverter.GetBytes((ulong)texture.Length - 20));
            dds_raw.AddRange(BitConverter.GetBytes((uint)1));

            dds_raw.AddRange(new byte[0xC] { 0x46, 0x45, 0x54, 0x48, 0x2D, 0x44, 0x44, 0x53, 0x01, 0x00, 0x03, 0x00 });

            for (int i = 0; i < 0x20; i++)
                dds_raw.Add(0);

            dds_raw.AddRange(BitConverter.GetBytes((uint)32));
            dds_raw.AddRange(BitConverter.GetBytes((uint)4));
            dds_raw.AddRange(Encoding.ASCII.GetBytes(texture.Compression));

            for (int i = 0; i < 0x24; i++)
                dds_raw.Add(0);

            dds_raw.AddRange(buffer);

            fs.Close();
            return new MemoryStream(dds_raw.ToArray());
        }

        public Bitmap get_dds(int index)
        {

            MemoryStream stream = DDS_Stream(index);

            Bitmap bitmap = null;
            using (var image = Pfim.Pfim.FromStream(stream))
            {
                PixelFormat format;

                // Convert from Pfim's backend agnostic image format into GDI+'s image format
                switch (image.Format)
                {
                    case Pfim.ImageFormat.Rgba32:
                        format = PixelFormat.Format32bppArgb;
                        break;
                    case Pfim.ImageFormat.Rgb24:
                        format = PixelFormat.Format24bppRgb;
                        break;
                    default:
                        throw new NotImplementedException();
                }

                var handle = GCHandle.Alloc(image.Data, GCHandleType.Pinned);
                try
                {
                    var data = Marshal.UnsafeAddrOfPinnedArrayElement(image.Data, 0);
                    bitmap = new Bitmap(image.Width, image.Height, image.Stride, format, data);
                }
                finally
                {
                    handle.Free();
                }
            }
            return bitmap;
        }
    }

    public class THTexture
    {
        public string Compression;
        public double Width;
        public double Height;

        public uint Offset;
        public uint Length;

        public THTexture(FileStream fs, uint offset, uint length)
        {
            byte[] buffer = buffer = th_utils.read_seek(fs, (int)offset, 4);

            Offset = offset;
            Length = length;

            byte dds_type = buffer[1];
            byte dds_mask = buffer[2];

            switch (dds_type)
            {
                case 0x59:
                    Compression = "DXT1";
                    break;
                case 0x5B:
                    Compression = "DXT5";
                    break;
                default:
                    throw new ArgumentException("Unsupported compression. Byte Value: 0x" + dds_type.ToString("X2"), "ERR_READ_GT1");
            }

            Width = Math.Pow(2, (dds_mask & 0xF0) / 16);
            Height = Math.Pow(2, dds_mask & 0x0F);
        }

    }
}
