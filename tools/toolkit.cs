﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace th_hack_tools
{
    // This file is only for classes that have nothing to do with Three Houses hacking
    // Only things that are related to external stuff or project management should go here

    public class Toolkit_Project
    {
        public string title { get; set; }
        public string directory { get; set; }
        public DateTime last_access { get; set; }
        public string version { get; set; }
        public List<Toolkit_File> files { get; set; }

        public byte[] file_to_bytes(int index)
        {
            string path = get_file(index);
            return File.ReadAllBytes(path);
        }

        public string get_file(int index)
        {
            for (int i=0;i<files.Count;i++)
            {
                if (files[i].id == index)
                    return directory + files[i].path;
            }
            return "";
        }

    }

    public class Toolkit_File
    {
        public long id { get; set; }
        public string path { get; set; }
        public bool modified { get; set; }
    }

    public class Toolkit_Config
    {
        public List<string> last_projects { get; set; }
        public string sd_path { get; set; }
        public Config_FTP ftp { get; set; }

        public Toolkit_Config()
        {
            last_projects = new List<string>();
            sd_path = "";
            ftp = new Config_FTP();
        }

        public void Save()
        {
            string config_path = AppDomain.CurrentDomain.BaseDirectory + "config.json";
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(config_path, json);
        }

    }

    public class Config_FTP
    {
        public string server { get; set; }
        public string user { get; set; }
        public string password { get; set; }

        public Config_FTP()
        {
            server = "";
            user = "";
            password = "";
        }
    }

}
