﻿namespace th_hack_tools
{
    partial class Project_New_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Box_ROMFS = new System.Windows.Forms.TextBox();
            this.Box_Output = new System.Windows.Forms.TextBox();
            this.Button_ROMFS = new System.Windows.Forms.Button();
            this.Button_Output = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Button_Create = new System.Windows.Forms.Button();
            this.Progress_Extraction = new System.Windows.Forms.ProgressBar();
            this.Label_Progress = new System.Windows.Forms.Label();
            this.Dialog_Browse = new System.Windows.Forms.FolderBrowserDialog();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Worker_Extraction = new System.ComponentModel.BackgroundWorker();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Create a new Project";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Output Folder";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "ROMFS";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Box_ROMFS, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Box_Output, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Button_ROMFS, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Button_Output, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 46);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(464, 70);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // Box_ROMFS
            // 
            this.Box_ROMFS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Box_ROMFS.Location = new System.Drawing.Point(99, 10);
            this.Box_ROMFS.Name = "Box_ROMFS";
            this.Box_ROMFS.Size = new System.Drawing.Size(265, 21);
            this.Box_ROMFS.TabIndex = 3;
            this.Box_ROMFS.Text = "E:\\SwitchHacking\\XCI-Explorer-v1.4.7\\tools\\updatedgameromfs";
            this.Box_ROMFS.TextChanged += new System.EventHandler(this.Box_ROMFS_TextChanged);
            // 
            // Box_Output
            // 
            this.Box_Output.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Box_Output.Location = new System.Drawing.Point(99, 39);
            this.Box_Output.Name = "Box_Output";
            this.Box_Output.Size = new System.Drawing.Size(265, 21);
            this.Box_Output.TabIndex = 4;
            this.Box_Output.Text = "E:\\SwitchHacking\\XCI_NCA_NSP_v2\\output\\TOOLKIT";
            this.Box_Output.TextChanged += new System.EventHandler(this.Box_Output_TextChanged);
            // 
            // Button_ROMFS
            // 
            this.Button_ROMFS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_ROMFS.Location = new System.Drawing.Point(370, 9);
            this.Button_ROMFS.Name = "Button_ROMFS";
            this.Button_ROMFS.Size = new System.Drawing.Size(85, 23);
            this.Button_ROMFS.TabIndex = 5;
            this.Button_ROMFS.Text = "Open...";
            this.Button_ROMFS.UseVisualStyleBackColor = true;
            this.Button_ROMFS.Click += new System.EventHandler(this.Button_ROMFS_Click);
            // 
            // Button_Output
            // 
            this.Button_Output.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Output.Location = new System.Drawing.Point(370, 38);
            this.Button_Output.Name = "Button_Output";
            this.Button_Output.Size = new System.Drawing.Size(85, 23);
            this.Button_Output.TabIndex = 6;
            this.Button_Output.Text = "Open...";
            this.Button_Output.UseVisualStyleBackColor = true;
            this.Button_Output.Click += new System.EventHandler(this.Button_Output_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(464, 46);
            this.panel1.TabIndex = 4;
            // 
            // Button_Create
            // 
            this.Button_Create.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Button_Create.Location = new System.Drawing.Point(370, 214);
            this.Button_Create.Name = "Button_Create";
            this.Button_Create.Size = new System.Drawing.Size(85, 25);
            this.Button_Create.TabIndex = 5;
            this.Button_Create.Text = "Create";
            this.Button_Create.UseVisualStyleBackColor = true;
            this.Button_Create.Click += new System.EventHandler(this.Button_Create_Click);
            // 
            // Progress_Extraction
            // 
            this.Progress_Extraction.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Progress_Extraction.Location = new System.Drawing.Point(12, 215);
            this.Progress_Extraction.Name = "Progress_Extraction";
            this.Progress_Extraction.Size = new System.Drawing.Size(352, 23);
            this.Progress_Extraction.TabIndex = 6;
            // 
            // Label_Progress
            // 
            this.Label_Progress.AutoEllipsis = true;
            this.Label_Progress.AutoSize = true;
            this.Label_Progress.Location = new System.Drawing.Point(9, 195);
            this.Label_Progress.Name = "Label_Progress";
            this.Label_Progress.Size = new System.Drawing.Size(59, 15);
            this.Label_Progress.TabIndex = 7;
            this.Label_Progress.Text = "Progress:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(99, 122);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(356, 67);
            this.textBox1.TabIndex = 8;
            // 
            // Worker_Extraction
            // 
            this.Worker_Extraction.WorkerReportsProgress = true;
            this.Worker_Extraction.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Worker_Extraction_DoWork);
            this.Worker_Extraction.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Worker_Extraction_ProgressChanged);
            this.Worker_Extraction.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Worker_Extraction_RunWorkerCompleted);
            // 
            // New_Project_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 252);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Label_Progress);
            this.Controls.Add(this.Progress_Extraction);
            this.Controls.Add(this.Button_Create);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "New_Project_Form";
            this.Text = "New Project";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox Box_ROMFS;
        private System.Windows.Forms.TextBox Box_Output;
        private System.Windows.Forms.Button Button_ROMFS;
        private System.Windows.Forms.Button Button_Output;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Button_Create;
        private System.Windows.Forms.ProgressBar Progress_Extraction;
        private System.Windows.Forms.Label Label_Progress;
        private System.Windows.Forms.FolderBrowserDialog Dialog_Browse;
        private System.Windows.Forms.TextBox textBox1;
        private System.ComponentModel.BackgroundWorker Worker_Extraction;
    }
}