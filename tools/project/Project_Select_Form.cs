﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace th_hack_tools
{
    public partial class Project_Select_Form : Form
    {
        public Project_Select_Form()
        {
            InitializeComponent();
        }

        private void Button_Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog FileDialog = new OpenFileDialog
            {
                InitialDirectory = @"C:\",
                Title = "Open a project",

                CheckFileExists = true,
                CheckPathExists = true,

                Filter = "TH Project (*.thproject)|*.thproject",
                RestoreDirectory = true,

                ShowReadOnly = false
            };

            if (FileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    project_load(FileDialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "File opening error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void project_load(string file)
        {
            Program.project = JsonConvert.DeserializeObject<Toolkit_Project>(File.ReadAllText(file));

            if (Program.config.last_projects.Contains(file))
                Program.config.last_projects.Remove(file);
            Program.config.last_projects.Insert(0, file);

            Hide();
            using (MainForm main = new MainForm())
            {
                main.ShowDialog();
            }

        }

        private void Project_Select_Form_Load(object sender, EventArgs e)
        {
            foreach (string project in Program.config.last_projects)
            {
                Box_Projects.Items.Add(project);
            }
        }

        private void Box_Projects_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = ((ListBox)sender).IndexFromPoint(e.Location);
            if (index != ListBox.NoMatches)
            {
                string item = ((ListBox)sender).Items[index].ToString();
                if (File.Exists(item))
                {
                    project_load(item);
                }
                else
                {
                    Program.config.last_projects.RemoveAt(index);
                    ((ListBox)sender).Items.RemoveAt(index);
                    Program.config.Save();
                    MessageBox.Show("It seems the project file doesn't exist anymore.\nDid you move the file?", "Project doesn't exist.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

            }
        }

        private void Button_Create_Click(object sender, EventArgs e)
        {
            using (Project_New_Form new_project = new Project_New_Form())
            {
                new_project.ShowDialog();
            }
        }
    }

}
