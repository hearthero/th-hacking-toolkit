﻿using Ionic.Zlib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace th_hack_tools
{
    public partial class Project_New_Form : Form
    {
        public Project_New_Form()
        {
            InitializeComponent();
        }

        th_structure structure;
        string data_0;
        string data_1;

        private void create_check()
        {
            if (Box_ROMFS.Text != "" && Box_Output.Text != "")
            {
                Button_Create.Enabled = true;
            }
            else
            {
                Button_Create.Enabled = false;
            }
                
        }

        private void Box_ROMFS_TextChanged(object sender, EventArgs e)
        {
            create_check();
        }

        private void Box_Output_TextChanged(object sender, EventArgs e)
        {
            create_check();
        }

        private void Button_ROMFS_Click(object sender, EventArgs e)
        {
            if (Dialog_Browse.ShowDialog() == DialogResult.OK)
            {
                if (!Directory.Exists(Dialog_Browse.SelectedPath))
                    MessageBox.Show("Read error", "Directory doesn't exist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    Box_ROMFS.Text = Dialog_Browse.SelectedPath;
            }
        }

        private void Button_Output_Click(object sender, EventArgs e)
        {
            if (Dialog_Browse.ShowDialog() == DialogResult.OK)
            {
                if (!Directory.Exists(Dialog_Browse.SelectedPath))
                    MessageBox.Show("Read error", "Directory doesn't exist", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    Box_Output.Text = Dialog_Browse.SelectedPath;
            }
        }

        private void Button_Create_Click(object sender, EventArgs e)
        {

            data_0 = Box_ROMFS.Text + Path.DirectorySeparatorChar + "DATA0.bin";
            data_1 = Box_ROMFS.Text + Path.DirectorySeparatorChar + "DATA1.bin";
            structure = new th_structure(data_0);

            this.Enabled = false;
            Worker_Extraction.RunWorkerAsync();

        }

        private void Worker_Extraction_DoWork(object sender, DoWorkEventArgs e)
        {

            // Create a new project file

            Toolkit_Project project = new Toolkit_Project
            {
                directory = Box_Output.Text,
                last_access = DateTime.Now,
                title = "Testproject",
                version = Program.version,
                files = new List<Toolkit_File>()
            };

            // Iterate through DATA1 contents

            for (int i = 0; i < structure.data.Count; i++)
            {
                Worker_Extraction.ReportProgress((int)((decimal)i / (decimal)structure.data.Count * 100 / 2), "Extracting DATA1: " + i.ToString("X8") + ".bin");

                if (structure.data[i].size == 0)
                    continue;

                string filename = Box_Output.Text + Path.DirectorySeparatorChar + "DATA1" + Path.DirectorySeparatorChar + i.ToString("X8") + ".bin";
                Directory.CreateDirectory(Box_Output.Text + Path.DirectorySeparatorChar + "DATA1");

                data1_extract(filename, structure.data[i]);

                if (structure.data[i].is_zip)
                    using (FileStream originalFileStream = new FileStream(filename + ".gz", FileMode.Open, FileAccess.Read))
                    {
                        th_utils.decompress_file(originalFileStream, filename);
                        File.Delete(filename + ".gz");
                    }

                string extension = th_utils.analyze_extension(filename);
                File.Move(filename, Path.ChangeExtension(filename, extension));
                filename = Path.ChangeExtension(filename, extension).Replace(Box_Output.Text, "");

                // Prepare data for toolkit file

                Toolkit_File current = new Toolkit_File
                {
                    id = i,
                    modified = false,
                    path = filename
                };
                project.files.Add(current);

            }

            // Iterate through patch contents

            int current_patchfile = 0;
            foreach (KeyValuePair<int, block_patch> patchfile in structure.patched_data)
            {
                current_patchfile++;
                string file = patchfile.Value.path.Replace("rom:/", Path.DirectorySeparatorChar.ToString());
                file = file.Replace('/', Path.DirectorySeparatorChar);

                Worker_Extraction.ReportProgress((int)((decimal)current_patchfile / (decimal)structure.patched_data.Count * 50) + 50, "Extracting Patch Data: " + file);
                new FileInfo(Box_Output.Text + file).Directory.Create();

                string original_file = structure.path + file;

                if (patchfile.Value.is_zip)
                {
                    using (FileStream originalFileStream = new FileStream(original_file, FileMode.Open, FileAccess.Read))
                    {
                        file = file.Remove(file.Length - 3);
                        th_utils.decompress_file(originalFileStream, Box_Output.Text + file);
                    }
                }
                else
                {
                    File.Copy(structure.path + file, Box_Output.Text + file, true);
                }

                // Prepare data for toolkit file

                Toolkit_File current = new Toolkit_File
                {
                    id = patchfile.Value.index,
                    modified = false,
                    path = file
                };

                if (project.files.ElementAtOrDefault((int)current.id) != null)
                    project.files[(int)current.id] = current;
                else
                    project.files.Add(current);

            }

            string json = JsonConvert.SerializeObject(project, Formatting.Indented);
            File.WriteAllText(Box_Output.Text + Path.DirectorySeparatorChar + project.title + ".thproject",json);

        }

        private void data1_extract(string filename, block_data chunk)
        {
            using (FileStream datastream = new FileStream(data_1, FileMode.Open, FileAccess.Read))
            {
                if (chunk.is_zip)
                    filename = filename + ".gz";

                datastream.Seek(chunk.offset, SeekOrigin.Begin);
                using (FileStream writer = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {

                    int bufferSize = 1024;
                    byte[] buffer = new byte[bufferSize];

                    while (writer.Length + 1024 < chunk.size_zipped)
                    {
                        datastream.Read(buffer, 0, bufferSize);
                        writer.Write(buffer, 0, 1024);
                    }

                    while (writer.Length < chunk.size_zipped)
                    {
                        datastream.Read(buffer, 0, 1);
                        writer.WriteByte(buffer[0]);
                    }

                    writer.Close();
                }
                datastream.Close();
            }
        }

        private void Worker_Extraction_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progress_Extraction.Value = e.ProgressPercentage;
            Label_Progress.Text = (string)e.UserState;
        }

        private void Worker_Extraction_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show("Extraction complete", "All files have been successfully extracted.", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
