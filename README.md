This is the most recent version of the Three Houses hacking toolkit although it hasn't been worked on for over 1 year.
The hacking toolkit is Open Source and you can do whatever you want with it but I won't give any support anymore.

In comparison to the last public release this includes a patch loader, a texture viewer and audio tools for KTLSBIN, Koei Tecmo Sound Stream (KTSS) and Switch OPUS.

Sadly, trying to innovate hacking of very recent games teached me that there is way less respect amongst rom hackers that hack modern games specifically. It seems almost everyone just wants a piece of the cake and doesn't seek fun but fame instead. There have been people that outright stole stuff and even people that threatened me by DM... 

I hate drama so I kept quiet for the sake of harmony. But I really don't want to create tools for recent games any time soon. I hope you understand.
