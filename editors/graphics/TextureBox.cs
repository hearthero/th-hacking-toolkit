﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using th_hack_tools.tools;

namespace th_hack_tools.editors.graphics
{
    class TextureBox : PictureBox
    {
        public GT1 file { get; set;}
        public int index { get; set; }

        public void load_texture(int _index)
        {
            index = _index;
            Image = file.get_dds(index);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            if (file != null)
            {
                using (editors.graphics.Graphics_Editor editor = new editors.graphics.Graphics_Editor(file.file, index))
                {
                    editor.ShowDialog();
                }
            }
        }

    }
}
