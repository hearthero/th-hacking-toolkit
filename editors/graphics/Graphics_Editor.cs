﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;
using th_hack_tools.tools;

namespace th_hack_tools.editors.graphics
{

    public partial class Graphics_Editor : Form
    {

        GT1 graphic;
        string file = "";
        bool control_access = false;
        int current_texture = 0;

        public Graphics_Editor()
        {
            InitializeComponent();
        }

        public Graphics_Editor(string _file, int index = 0)
        {
            file = _file;
            control_access = true;
            current_texture = index;
            InitializeComponent();
        }

        private void Graphics_Editor_Load(object sender, EventArgs e)
        {
            if (control_access)
            {
                Menu_Open.Enabled = false;
                graphic = new GT1(file);
                load_details();
            }
        }

        private void load_details()
        {
            Box_CurrentTexture.BeginUpdate();
            for (int i = 0; i < graphic.texture_count; i++)
                Box_CurrentTexture.Items.Add("0x" + i.ToString("X4"));
            Box_CurrentTexture.EndUpdate();

            Label_TextureCount.Text = "Texture Count: " + graphic.texture_count;
            Box_CurrentTexture.SelectedIndex = current_texture;

            Menu_Export.Enabled = true;
            Menu_Import.Enabled = true;
            Menu_Copy.Enabled = true;
            Menu_Paste.Enabled = true;

            Box_CurrentTexture.Focus();
        }

        private void Menu_Open_Click(object sender, EventArgs e)
        {
            if (Dialog_Open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    Box_CurrentTexture.Items.Clear();
                    file = Dialog_Open.FileName;
                    graphic = new GT1(file);

                    load_details();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "File opening error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void Graphics_Editor_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void Box_CurrentTexture_SelectedIndexChanged(object sender, EventArgs e)
        {
            current_texture = Box_CurrentTexture.SelectedIndex;
            Label_Info.Text = graphic.textures[current_texture].Compression + " Size: " + graphic.textures[current_texture].Width + "x" + graphic.textures[current_texture].Height + "px";
            Picture_Texture.Image = graphic.get_dds(current_texture);
            Picture_Texture.Update();
        }

        private void Button_ViewZoom_Click(object sender, EventArgs e)
        {
            Button_ViewZoom.Checked = true;
            Button_ViewCenter.Checked = false;
            Picture_Texture.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void Button_ViewCenter_Click(object sender, EventArgs e)
        {
            Button_ViewZoom.Checked = false;
            Button_ViewCenter.Checked = true;
            Picture_Texture.SizeMode = PictureBoxSizeMode.CenterImage;
        }

        private void Button_Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Picture_Texture_Click(object sender, EventArgs e)
        {
            Box_CurrentTexture.Focus();
        }

        private void Menu_Copy_Click(object sender, EventArgs e)
        {

            using (MemoryStream stream = new MemoryStream())
            {
                Picture_Texture.Image.Save(stream, ImageFormat.Png);
                var data = new DataObject("PNG", stream);
                Clipboard.Clear();
                Clipboard.SetDataObject(data, true);
            }

        }

        private void Menu_Export_Click(object sender, EventArgs e)
        {
            Dialog_Export.FileName = file.Remove(file.Length - 4) + "_" + Box_CurrentTexture.SelectedItem;
            if (Dialog_Export.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    switch (Path.GetExtension(Dialog_Export.FileName))
                    {
                        case ".png":
                            Picture_Texture.Image.Save(Dialog_Export.FileName, ImageFormat.Png);
                            break;
                        case ".dds":
                            int index = Box_CurrentTexture.SelectedIndex;
                            MemoryStream stream = graphic.DDS_Stream(index);
                            using (FileStream writer = new FileStream(Dialog_Export.FileName, FileMode.Create, FileAccess.Write))
                            {
                                stream.WriteTo(writer);
                                writer.Close();
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "File saving error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }

}
