﻿namespace th_hack_tools.editors.graphics
{
    partial class Graphics_Editor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Graphics_Editor));
            this.Menu_Main = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Open = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Import = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_View = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_ViewZoom = new System.Windows.Forms.ToolStripMenuItem();
            this.Button_ViewCenter = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Copy = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Paste = new System.Windows.Forms.ToolStripMenuItem();
            this.Picture_Texture = new System.Windows.Forms.PictureBox();
            this.Statusbar = new System.Windows.Forms.ToolStrip();
            this.Label_Info = new System.Windows.Forms.ToolStripLabel();
            this.Label_TextureCount = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.Box_CurrentTexture = new System.Windows.Forms.ToolStripComboBox();
            this.Dialog_Open = new System.Windows.Forms.OpenFileDialog();
            this.Dialog_Export = new System.Windows.Forms.SaveFileDialog();
            this.Menu_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_Texture)).BeginInit();
            this.Statusbar.SuspendLayout();
            this.SuspendLayout();
            // 
            // Menu_Main
            // 
            this.Menu_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.Menu_View,
            this.Menu_Copy,
            this.Menu_Paste});
            this.Menu_Main.Location = new System.Drawing.Point(0, 0);
            this.Menu_Main.Name = "Menu_Main";
            this.Menu_Main.Size = new System.Drawing.Size(624, 24);
            this.Menu_Main.TabIndex = 1;
            this.Menu_Main.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Open,
            this.Menu_Export,
            this.Menu_Import,
            this.Menu_Close});
            this.fileToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("fileToolStripMenuItem.Image")));
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // Menu_Open
            // 
            this.Menu_Open.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Open.Image")));
            this.Menu_Open.Name = "Menu_Open";
            this.Menu_Open.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.Menu_Open.Size = new System.Drawing.Size(155, 22);
            this.Menu_Open.Text = "Open...";
            this.Menu_Open.Click += new System.EventHandler(this.Menu_Open_Click);
            // 
            // Menu_Export
            // 
            this.Menu_Export.Enabled = false;
            this.Menu_Export.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Export.Image")));
            this.Menu_Export.Name = "Menu_Export";
            this.Menu_Export.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.Menu_Export.Size = new System.Drawing.Size(155, 22);
            this.Menu_Export.Text = "Export";
            this.Menu_Export.Click += new System.EventHandler(this.Menu_Export_Click);
            // 
            // Menu_Import
            // 
            this.Menu_Import.Enabled = false;
            this.Menu_Import.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Import.Image")));
            this.Menu_Import.Name = "Menu_Import";
            this.Menu_Import.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.Menu_Import.Size = new System.Drawing.Size(155, 22);
            this.Menu_Import.Text = "Import";
            // 
            // Menu_Close
            // 
            this.Menu_Close.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Close.Image")));
            this.Menu_Close.Name = "Menu_Close";
            this.Menu_Close.Size = new System.Drawing.Size(155, 22);
            this.Menu_Close.Text = "Close";
            this.Menu_Close.Click += new System.EventHandler(this.Button_Close_Click);
            // 
            // Menu_View
            // 
            this.Menu_View.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Button_ViewZoom,
            this.Button_ViewCenter});
            this.Menu_View.Image = ((System.Drawing.Image)(resources.GetObject("Menu_View.Image")));
            this.Menu_View.Name = "Menu_View";
            this.Menu_View.Size = new System.Drawing.Size(60, 20);
            this.Menu_View.Text = "View";
            // 
            // Button_ViewZoom
            // 
            this.Button_ViewZoom.Checked = true;
            this.Button_ViewZoom.CheckOnClick = true;
            this.Button_ViewZoom.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Button_ViewZoom.Name = "Button_ViewZoom";
            this.Button_ViewZoom.Size = new System.Drawing.Size(145, 22);
            this.Button_ViewZoom.Text = "Zoom";
            this.Button_ViewZoom.Click += new System.EventHandler(this.Button_ViewZoom_Click);
            // 
            // Button_ViewCenter
            // 
            this.Button_ViewCenter.CheckOnClick = true;
            this.Button_ViewCenter.Name = "Button_ViewCenter";
            this.Button_ViewCenter.Size = new System.Drawing.Size(145, 22);
            this.Button_ViewCenter.Text = "Center Image";
            this.Button_ViewCenter.Click += new System.EventHandler(this.Button_ViewCenter_Click);
            // 
            // Menu_Copy
            // 
            this.Menu_Copy.Enabled = false;
            this.Menu_Copy.Image = ((System.Drawing.Image)(resources.GetObject("Menu_Copy.Image")));
            this.Menu_Copy.Name = "Menu_Copy";
            this.Menu_Copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.Menu_Copy.Size = new System.Drawing.Size(99, 20);
            this.Menu_Copy.Text = "Copy Image";
            this.Menu_Copy.Click += new System.EventHandler(this.Menu_Copy_Click);
            // 
            // Menu_Paste
            // 
            this.Menu_Paste.Enabled = false;
            this.Menu_Paste.Name = "Menu_Paste";
            this.Menu_Paste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.Menu_Paste.Size = new System.Drawing.Size(83, 20);
            this.Menu_Paste.Text = "Paste Image";
            // 
            // Picture_Texture
            // 
            this.Picture_Texture.BackColor = System.Drawing.Color.Gray;
            this.Picture_Texture.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Picture_Texture.BackgroundImage")));
            this.Picture_Texture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Picture_Texture.Location = new System.Drawing.Point(0, 24);
            this.Picture_Texture.Name = "Picture_Texture";
            this.Picture_Texture.Size = new System.Drawing.Size(624, 388);
            this.Picture_Texture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Picture_Texture.TabIndex = 4;
            this.Picture_Texture.TabStop = false;
            this.Picture_Texture.Click += new System.EventHandler(this.Picture_Texture_Click);
            // 
            // Statusbar
            // 
            this.Statusbar.CanOverflow = false;
            this.Statusbar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Statusbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.Statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Label_Info,
            this.Label_TextureCount,
            this.toolStripSeparator2,
            this.toolStripLabel3,
            this.Box_CurrentTexture});
            this.Statusbar.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.Statusbar.Location = new System.Drawing.Point(0, 412);
            this.Statusbar.Name = "Statusbar";
            this.Statusbar.Padding = new System.Windows.Forms.Padding(3);
            this.Statusbar.Size = new System.Drawing.Size(624, 29);
            this.Statusbar.TabIndex = 5;
            this.Statusbar.Text = "toolStrip1";
            // 
            // Label_Info
            // 
            this.Label_Info.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.Label_Info.Name = "Label_Info";
            this.Label_Info.Size = new System.Drawing.Size(86, 20);
            this.Label_Info.Text = "No File Loaded";
            // 
            // Label_TextureCount
            // 
            this.Label_TextureCount.Name = "Label_TextureCount";
            this.Label_TextureCount.Size = new System.Drawing.Size(93, 20);
            this.Label_TextureCount.Text = "Texture Count: 0";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 23);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(94, 20);
            this.toolStripLabel3.Text = "Current Texture: ";
            // 
            // Box_CurrentTexture
            // 
            this.Box_CurrentTexture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Box_CurrentTexture.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.Box_CurrentTexture.Name = "Box_CurrentTexture";
            this.Box_CurrentTexture.Size = new System.Drawing.Size(121, 23);
            this.Box_CurrentTexture.SelectedIndexChanged += new System.EventHandler(this.Box_CurrentTexture_SelectedIndexChanged);
            // 
            // Dialog_Open
            // 
            this.Dialog_Open.Filter = "G1T files (*.g1t)|*.g1t";
            this.Dialog_Open.Title = "Import GT1 graphic";
            // 
            // Dialog_Export
            // 
            this.Dialog_Export.Filter = "PNG file (*.png)|*.png|DDS file (*.dds)|*.dds";
            // 
            // Graphics_Editor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.Picture_Texture);
            this.Controls.Add(this.Statusbar);
            this.Controls.Add(this.Menu_Main);
            this.MainMenuStrip = this.Menu_Main;
            this.Name = "Graphics_Editor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Graphics Editor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Graphics_Editor_FormClosing);
            this.Load += new System.EventHandler(this.Graphics_Editor_Load);
            this.Menu_Main.ResumeLayout(false);
            this.Menu_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Picture_Texture)).EndInit();
            this.Statusbar.ResumeLayout(false);
            this.Statusbar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip Menu_Main;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Menu_Open;
        private System.Windows.Forms.ToolStripMenuItem Menu_Export;
        private System.Windows.Forms.ToolStripMenuItem Menu_Close;
        private System.Windows.Forms.ToolStripMenuItem Menu_Copy;
        private System.Windows.Forms.ToolStripMenuItem Menu_Paste;
        private System.Windows.Forms.ToolStripMenuItem Menu_Import;
        private System.Windows.Forms.PictureBox Picture_Texture;
        private System.Windows.Forms.ToolStrip Statusbar;
        private System.Windows.Forms.ToolStripLabel Label_Info;
        private System.Windows.Forms.ToolStripLabel Label_TextureCount;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox Box_CurrentTexture;
        private System.Windows.Forms.ToolStripMenuItem Menu_View;
        private System.Windows.Forms.ToolStripMenuItem Button_ViewZoom;
        private System.Windows.Forms.ToolStripMenuItem Button_ViewCenter;
        private System.Windows.Forms.OpenFileDialog Dialog_Open;
        private System.Windows.Forms.SaveFileDialog Dialog_Export;
    }
}