﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace th_hack_tools
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string config_path = AppDomain.CurrentDomain.BaseDirectory + "config.json";

            if (File.Exists(config_path))
            {
                config = JsonConvert.DeserializeObject<Toolkit_Config>(File.ReadAllText(config_path));
            }
            else
            {
                config = new Toolkit_Config();
                config.Save();
            }

            Application.Run(new Project_Select_Form());
        }

        public static string version = "1.0";
        public static Toolkit_Config config;
        public static Toolkit_Project project;
    }
}
