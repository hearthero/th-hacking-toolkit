﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace th_hack_tools
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void SaveCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            worker.Dispose();
            MessageBox.Show("Your changes have been saved.", "File saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Progress_Save.Value = 0;

        }

        private void SaveProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Progress_Save.Value = e.ProgressPercentage;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://github.com/HeartHeroDE/th-hack-tools");
        }

        private void Button_CharacterEditor_Click(object sender, EventArgs e)
        {
            using (Character_Editor editor = new Character_Editor())
            {
                editor.ShowDialog();
            }
        }

        private void Button_ClassEditor_Click(object sender, EventArgs e)
        {
            using (Class_Editor editor = new Class_Editor())
            {
                editor.ShowDialog();
            }
        }

        private void Button_ItemEditor_Click(object sender, EventArgs e)
        {
            //using (editors.Text_Editor editor = new editors.Text_Editor())
            //{
            //    editor.ShowDialog();
            //}
        }

        private void Button_KTSL2STBIN_Extractor_Click(object sender, EventArgs e)
        {
            using (editors.audio.KTSL2STBIN_Extractor editor = new editors.audio.KTSL2STBIN_Extractor())
            {
                editor.ShowDialog();
            }
        }

        private void Button_KTSS_Exporter_Click(object sender, EventArgs e)
        {
            using (editors.audio.KTSS_Interpreter editor = new editors.audio.KTSS_Interpreter())
            {
                editor.ShowDialog();
            }
        }

        private void Button_Graphics_Editor_Click(object sender, EventArgs e)
        {
            using (editors.graphics.Graphics_Editor editor = new editors.graphics.Graphics_Editor())
            {
                editor.ShowDialog();
            }
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.config.Save();
            Application.Exit();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Text = Program.project.title + " - " + "TH Hacking Toolkit";
        }
    }
}
